#ifndef KEY_ANALYZER_H
#define KEY_ANALYZER_H

#include <stdbool.h>
#include "key_info.h"

key_info ** find_ssh_keyfiles(int* count);

int extract_ssh_keyfile_info(char *filepath, key_info** info);

bool is_known_hosts_file(char *filename);

int get_keyfile_algorithm(char *ssh_keygen_output, char **algorithm);

int get_keyfile_key_size(char *ssh_keygen_output, char **key_size);

int get_keyfile_owner(char *filepath, char **owner);

int get_keyfile_pq_secure(char** pq_secure);


#endif