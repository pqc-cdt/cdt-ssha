#include "key_analyzer.h"
#include "utils.h"
#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <json-c/json.h>

int main(int argc, char **argv) {

    // Ensure cdt-ssha is running with root privileges
    if (!is_root()) {
        fprintf(stderr, "This program must be run as root.\n");
        exit(1);
    }

    int key_count = 0;
    key_info **keys = find_ssh_keyfiles(&key_count);

    // Print information of keys
    if (key_count == 0) {
        exit(0);
    }
    printf("Located %i SSH keyfiles.\n", key_count);

    // Store obtained information in json format
    struct json_object *root = json_object_new_object();
    struct json_object *key_arr = json_object_new_array();

    for (int i = 0; i < key_count; i++) {
        struct json_object *key_obj = json_object_new_object();
        json_object_object_add(key_obj, "file_path", json_object_new_string(keys[i]->file_path));
        json_object_object_add(key_obj, "owner", json_object_new_string(keys[i]->owner));
        json_object_object_add(key_obj, "algorithm", json_object_new_string(keys[i]->algorithm));
        json_object_object_add(key_obj, "key_size", json_object_new_string(keys[i]->key_size));
        json_object_object_add(key_obj, "pq_secure", json_object_new_string(keys[i]->pq_secure));
        json_object_array_add(key_arr, key_obj);
    }
    json_object_object_add(root, "ssh_keys", key_arr);
    json_object_object_add(root, "ssh_key_count", json_object_new_int(key_count));
    const char *json_str = json_object_to_json_string_ext(root, JSON_C_TO_STRING_PRETTY);

    // Write json data to file
    FILE *file = fopen("report.sshreport", "w");
    if (file != NULL) {
        fputs(json_str, file);
        fclose(file);
        printf("JSON export completed successfully.\n");
    } else {
        printf("Error opening the file for writing.\n");
    }
    json_object_put(root);

    // Free resources
    for (int i = 0; i < key_count; ++i) {
        free(keys[i]->file_path);
        free(keys[i]->owner);
        free(keys[i]->algorithm);
        free(keys[i]->key_size);
        free(keys[i]->pq_secure);
        free(keys[i]);
    }
    free(keys);

    return 0;
}