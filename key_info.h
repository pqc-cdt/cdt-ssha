#ifndef KEY_INFO_H
#define KEY_INFO_H

#include <stdint.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct key_info {
    char *file_path;
    char *owner;
    char *algorithm;
    char *key_size;
    char *pq_secure;
} key_info;

void key_info_print(key_info *info);

#endif