#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <unistd.h>
#include "key_info.h"

/**
 * @brief Checks if the current user is root.
 *
 * @return True if the current user is root, false otherwise.
 */
bool is_root();

/**
 * @brief Runs an external command and returns the output as a string.
 *
 * @param command_string The command to run.
 * @param exit_code A pointer to an integer that will be set to the exit code of the command.
 * @return A pointer to the output of the command, or NULL if the command failed.
 */
char *run_external_command(const char *command_string, int *exit_code);

int extract_owner_information(const char *file_path, char **owner_info);

key_info *extract_algorithm_and_key_size(const char *input, const char *pattern);

int get_regex_capture_group(const char *input, const char *pattern, char **matched_value);

#endif