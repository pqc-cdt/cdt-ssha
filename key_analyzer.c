#include "key_analyzer.h"
#include "utils.h"
#include <dirent.h>
#include <pwd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <grp.h>

key_info **find_ssh_keyfiles(int *count) {
    struct passwd *user;

    key_info **keys = calloc(1, sizeof(key_info *));
    if (!keys) { exit(1); }
    int key_cap = 1;
    int key_count = 0;

    // Iterate over all users
    while ((user = getpwent()) != NULL) {
        // Check if the user has a home directory
        if (user->pw_dir != NULL) {
            // Check if the user has a .ssh directory
            DIR *ssh_dir = opendir(strcat(user->pw_dir, "/.ssh"));
            if (ssh_dir != NULL) {
                // Iterate over files in .ssh directory
                struct dirent *dir_entry;
                while ((dir_entry = readdir(ssh_dir)) != NULL) {
                    // Check if the file is a regular file
                    if (dir_entry->d_type == DT_REG) {
                        // Skip known_hosts files
                        if (!is_known_hosts_file(dir_entry->d_name)) {

                            // Construct file path
                            char *ssh_filepath = calloc(PATH_MAX, sizeof(char));
                            snprintf(ssh_filepath, PATH_MAX, "%s/%s", user->pw_dir, dir_entry->d_name);

                            // Extract information from key file
                            key_info *key = calloc(1, sizeof(key_info));
                            if (extract_ssh_keyfile_info(ssh_filepath, &key) == 0) {

                                // Add key to arr array, and resize if necessary
                                if (key_count == key_cap) {
                                    keys = realloc(keys, (key_count + 1) * sizeof(key_info *));
                                    key_cap++;
                                }

                                if (!keys) { exit(1); }
                                keys[key_count] = key;
                                key_count++;
                            } else {
                                // If the file was not a valid key file, free allocated resources
                                free(ssh_filepath);
                                free(key);
                            }
                        }
                    }
                }
                closedir(ssh_dir);
            }
        }
    }
    endpwent();
    *count = key_count;
    return keys;
}

bool is_known_hosts_file(char *filename) {
    return strcmp(filename, "known_hosts") == 0 || strcmp(filename, "known_hosts.old") == 0;
}

int extract_ssh_keyfile_info(char *file_path, key_info **info_out) {

    // Construct command from keyfile path
    char cmd[1024];
    snprintf(cmd, sizeof(cmd), "ssh-keygen -l -f %s 2>/dev/null", file_path);

    // Execute command to extract information from keyfile
    int exit_code;
    char *cmd_output = run_external_command(cmd, &exit_code);
    if (exit_code != 0 || !cmd_output) {
        free(cmd_output);
        return 1;
    }

    // Extract the algorithm and key size from the command output.
    int res1 = get_keyfile_algorithm(cmd_output, &((*info_out)->algorithm));
    int res2 = get_keyfile_key_size(cmd_output, &((*info_out)->key_size));
    int res3 = get_keyfile_owner(file_path, &((*info_out)->owner));
    int res4 = get_keyfile_pq_secure(&((*info_out)->pq_secure));
    (*info_out)->file_path = file_path;

    // Ensure all the info could be obtained
    if (res1 < 0 || res2 < 0 || res3 < 0 || res4 < 0) {
        exit(1);
    }

    free(cmd_output);
    return 0;
}

int get_keyfile_algorithm(char *ssh_keygen_output, char **algorithm) {
    const char *pattern = "^[[:digit:]]+[[:space:]][^[:space:]]+:[^[:space:]]+[[:space:]].*\\(([^[:space:]]+)\\).*$";
    char *regex_match = NULL;
    if (get_regex_capture_group(ssh_keygen_output, pattern, &regex_match) == 0) {
        *algorithm = regex_match;
        return 0;
    }
    return 1;
}

int get_keyfile_key_size(char *ssh_keygen_output, char **key_size) {
    const char *pattern = "^([[:digit:]]+)[[:space:]][^[:space:]]+:[^[:space:]]+[[:space:]].*\\([^[:space:]]+\\).*$";
    char *regex_match = NULL;
    if (get_regex_capture_group(ssh_keygen_output, pattern, &regex_match) == 0) {
        *key_size = regex_match;
        return 0;
    }
    return 1;
}

int get_keyfile_owner(char *file_path, char **owner_info) {
    struct stat file_stat;
    if (stat(file_path, &file_stat) < 0) {
        return 1;
    }

    struct passwd *owner = getpwuid(file_stat.st_uid);
    struct group *group = getgrgid(file_stat.st_gid);
    if (!owner || !group) {
        return 1;
    }

    size_t owner_len = strlen(owner->pw_name);
    size_t group_len = strlen(group->gr_name);
    size_t total_len = owner_len + 1 + group_len + 1;
    *owner_info = malloc(total_len);
    if (*owner_info == NULL) {
        return 1;
    }

    // Write owner and group into owner_info
    memcpy(*owner_info, owner->pw_name, owner_len);
    (*owner_info)[owner_len] = ':';
    memcpy((*owner_info) + owner_len + 1, group->gr_name, group_len);
    (*owner_info)[total_len - 1] = '\0';

    return 0;
}

int get_keyfile_pq_secure(char **pq_secure) {
    char *value = "false";
    *pq_secure = calloc(1, strlen(value) + 1);
    if (*pq_secure == NULL) {
        return 1;
    }
    memcpy(*pq_secure, value, strlen(value) + 1);
    return 0;
}
