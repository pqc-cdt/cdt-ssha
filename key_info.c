#include "key_info.h"
#include <stdio.h>

void key_info_print(key_info *info) {
    printf("Keyfile: %s\n", info->file_path);
    printf("  Algorithm: %s\n", info->algorithm);
    printf("  Key size: %s\n", info->key_size);
    printf("  Owner: %s\n", info->owner);
    printf("  PQ-Secure: %s\n", info->pq_secure ? "true" : "false");
}