#include "utils.h"
#include <linux/limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <threads.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <regex.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>

bool is_root() {
    return getuid() == 0;
}

char *run_external_command(const char *command_string, int *exit_code) {
    // Create a new stream connected to a pipe running the given command.
    FILE *fp = popen(command_string, "r");
    if (fp == NULL) {
        return NULL;
    }

    size_t bytes_read = 0;
    size_t buffer_size = 0;
    size_t buffer_offset = 0;
    size_t total_bytes_read = 0;
    char *buffer = NULL;

    while (1) {
        // Grow buffer to hold output of external command
        buffer = (char *) realloc((void *) buffer, buffer_size + 64);
        if (!buffer) {
            pclose(fp);
            return NULL;
        }
        buffer_size += 64;

        // Read the output of the external command into the buffer
        bytes_read = fread(buffer + buffer_offset, sizeof(char), buffer_size - buffer_offset, fp);
        total_bytes_read += bytes_read;
        if (bytes_read == 0) {
            break;
        }

        // Update buffer offset
        buffer_offset += 64;
    }

    buffer[total_bytes_read] = '\0';
    int res = pclose(fp);
    *exit_code = WEXITSTATUS(res);

    return buffer;
}


int extract_owner_information(const char *file_path, char **owner_info) {
    struct stat file_stat;
    if (stat(file_path, &file_stat) < 0) {
        return 1;
    }

    struct passwd *owner = getpwuid(file_stat.st_uid);
    struct group *group = getgrgid(file_stat.st_gid);
    if (!owner || !group) {
        return 1;
    }

    size_t owner_len = strlen(owner->pw_name);
    size_t group_len = strlen(group->gr_name);
    size_t total_len = owner_len + 1 + group_len + 1;
    *owner_info = malloc(total_len);
    if (*owner_info == NULL) {
        return 1;
    }

    // Write owner and group into owner_info
    memcpy(*owner_info, owner->pw_name, owner_len);
    (*owner_info)[owner_len] = ':';
    memcpy((*owner_info) + owner_len + 1, group->gr_name, group_len);
    (*owner_info)[total_len - 1] = '\0';

    return 0;
}

int get_regex_capture_group(const char *input, const char *pattern, char **matched_value) {
    regex_t regex;
    regmatch_t matches[2];
    *matched_value = NULL;

    if (!input || !pattern) {
        return 1;
    }

    // Compile regex pattern
    if (regcomp(&regex, pattern, REG_EXTENDED) != 0) {
        return 1;
    }

    // Find matches in the input
    int result = regexec(&regex, input, 2, matches, 0);
    switch (result) {
        case REG_NOMATCH: {
            regfree(&regex);
            return 1;
        }
        case REG_NOERROR: {
            *matched_value = calloc(matches[1].rm_eo - matches[1].rm_so + 1, sizeof(char));
            if (*matched_value == NULL) {
                regfree(&regex);
                return 1;
            }
            if (matches[1].rm_so == -1 && matches[1].rm_eo == -1) {
                free(*matched_value);
                regfree(&regex);
                *matched_value = NULL;
                return 1;
            }
            memcpy(*matched_value, input + matches[1].rm_so, matches[1].rm_eo - matches[1].rm_so);
            regfree(&regex);
            return 0;
        }
        default: {
            regfree(&regex);
            return 1;
        }
    }
}
