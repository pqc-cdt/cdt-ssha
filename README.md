# cdt-ssha
## Introduction

Quantum computers are a rapidly advancing area of research with the potential to revolutionize computing as we know it. 
However, their ability to quickly solve certain types of mathematical problems could also pose a significant threat to current cryptographic systems.
The Crypto-Detection-Tool project aims to provide a comprehensive suite of tools designed to assist system administrators in evaluating the resilience of the cryptographic components installed on a Linux system against potential threats posed by post-quantum computers

## Summary

This tool, cdt-ssha, is part of the crypto-detection-tool project.
It will search for SSH-Keys stored on typical locations on disk and analyze the used algorithm as well as the key length.

## Features

Currently, cdt-ssha provides the following features:

- Find SSH-Keys stored on the system
- Analyze Key-Type
- Analyze Key-Length


## How does it work?

1. Key-Files are aggregated by iterating thorugh users home directories and iterating through the files inside the .ssh folder.
2. An SSH-Key-fingerprint is created using the ssh-keygen command.
3. The algorithm-name and key-length is extracted from the fingerprint.
4. The file-owner is determined using the stat system-call.
5. An entry is written into a json based report file.

## Future work

- Support other stored crypto as e.g. certificates.

The list is endless...


## Dependencies:

- [json-c](https://github.com/json-c/json-c)
